/**
 * SPDX-License-Identifier: BSD-3-Clause
 * (c) David Alan Gilbert 2021, dave@treblig.org
 */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "pico/stdlib.h"
#include "hardware/adc.h"
#include "hardware/dma.h"
#include "hardware/pwm.h"

#include "defs.h"
#include "speech_words.h"

static const uint LED_PIN = PICO_DEFAULT_LED_PIN;

static uint32_t round2digit(uint32_t in)
{
  // I originally tried to do this using scanf %0.2g but found the pico
  // libc doesn't seem to support it properly
  unsigned ires;

  char buf[16];
  float res=(float)in;
  sprintf(buf, "%u", in);
  int len = strlen(buf);
  if (len > 1) {
    float pw = powf(10.0, len-2);
    float rou = pw / 2.0;
    unsigned modval = (unsigned)pw;
    res+=rou;
    ires = (unsigned)res;
    ires -= ires % modval;
  } else {
    ires = (unsigned)res;
  }

  return ires;
}

int main()
{
    stdio_init_all();

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    setup_sound();

    clear_sounds();
    add_sound(&word_oh);
    add_sound(&word_me);
    add_sound(&word_oh);
    add_sound(&word_my);
    add_sound(&sound_off);
    play_sound_queue();

    ohmeter_init();

    while (sound_busy())
        tight_loop_contents();

    while (true) {
      uint32_t result = read_ohmeter();
      printf("result: 0x%08x / %d\n", result, result);
      clear_sounds();
      add_sound(&sound_on);

      switch (result) {
        case OHMETER_OPEN:
          add_sound(&word_open);
          break;

        case OHMETER_SHORT:
          add_sound(&word_short);
          break;

        default:
          // We're never getting more than 2 digits precision
          result = round2digit(result);

          if (result >= 10*1000*1000) {
            // e.g. 25 Meg
            // (I.m not handling 1.3M as 1M3 or 1 point)
            speak_number(result / 1000000);
            add_sound(&word_meg);
          } else if (result >= 10*1000) {
            // e.g. 25 Kay
            speak_number(result / 1000);
            add_sound(&word_kay);
          } else if (result >= 1000 && result < 10*1000) {
            // e.g. 4 Kay 7 or 1 Kay
            speak_number(result / 1000);
            add_sound(&word_kay);
            uint32_t hundreds = (result % 1000) / 100;
            if (hundreds) {
              speak_number(hundreds);
            }
          } else if (result < 1000) {
            // e.g. 270 ohm
            speak_number(result);
            add_sound(&word_ohm);
          } else {
            speak_number(result);
          }
          break;
      }

      // Stop the PWM so it's quiet for the next reading
      add_sound(&sound_off);

      // Say it twice
      play_sound_queue();
      while (sound_busy())
          tight_loop_contents();
      sleep_ms(500);
      play_sound_queue();
      while (sound_busy())
          tight_loop_contents();
      sleep_ms(500);
    };
}
