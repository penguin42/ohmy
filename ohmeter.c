/**
 * SPDX-License-Identifier: BSD-3-Clause
 * (c) David Alan Gilbert 2021, dave@treblig.org
 */

/* Ohmeter specific code */

#include <stdio.h>

#include "hardware/adc.h"
#include "pico/stdlib.h"

#include "defs.h"

static const uint ADC_PIN = 26;
static const uint32_t OPEN_LIM = 0xf58; // From experiments
static const uint32_t SHORT_LIM = 0x020; // From experiments
static const double   NEXTRANGE_RATIO = 3.0; // TBD
static const uint32_t NEXTRANGE_LOW = 0x0100; // low ADC to go to next

static struct {uint gpio; uint32_t r;} ranges[] = {
  /* This list must be in descending order */
  { .gpio = 18, .r = 1*1000*1000 },
  { .gpio = 19, .r = 100*1000 },
  { .gpio = 20, .r = 10*1000 },
  { .gpio = 21, .r = 1000 },
  { .gpio = 0,  .r = 0 },
};

void ohmeter_init(void)
{
    adc_init();
    adc_gpio_init(ADC_PIN);
    adc_select_input(ADC_PIN-26);

    for (int i =0; ranges[i].r != 0; i++) {
        /* Turn off all of the GPIO base drives for now
         * This is based on what the adc init does; we want no
         * pullups or anything - ideally the reistors should be
         * pretty much disconnected.
         */
        gpio_init(ranges[i].gpio);
        gpio_set_dir(ranges[i].gpio, GPIO_IN);
        gpio_set_function(ranges[i].gpio, GPIO_FUNC_NULL);
        gpio_disable_pulls(ranges[i].gpio);
        gpio_set_input_enabled(ranges[i].gpio, false);
    }
}

static uint16_t adc_read_multi(void)
{
  const int TOCOUNT = 5;
  uint32_t total = 0;
  uint16_t min = 0xffff;
  uint16_t max = 0;

  printf("%s: ");
  for(int i = 0; i < TOCOUNT; i++) {
    uint16_t raw = adc_read();
    printf(" %04x ", raw);
    total+=raw;
    if (raw > max) max = raw;
    if (raw < min) min = raw;
    sleep_ms(3);
  }
  total /= TOCOUNT;
  printf(" : %04x spread=%04x\n", total, max-min);

  return total;
}

static uint32_t read_range(int r)
{
    // Turn the GPIO on
    uint gpio = ranges[r].gpio;
    gpio_set_function(gpio, GPIO_FUNC_SIO); // matches gpio_init
    gpio_set_dir(gpio, GPIO_OUT);
    gpio_put(gpio, 1);

    // Feels like we should wait a little while?
    sleep_ms(20);

    uint16_t adc = adc_read_multi();

    // and back off again
    gpio_set_dir(gpio, GPIO_IN);
    gpio_set_function(gpio, GPIO_FUNC_NULL);
    gpio_disable_pulls(gpio);
    gpio_set_input_enabled(gpio, false);

    return adc;
}

// Returns either integer ohms, or one of the OHMETER_ constants
uint32_t read_ohmeter(void)
{
    float res;

    for(int r = 0; ranges[r].r != 0; r++) {
        uint32_t adc = read_range(r);

        printf("Range: %d/%d adc: 0x%03x\n", r, ranges[r].r, adc);

        // If we're on the 1st (highest R) range, and we have a very
        // high reading, it's open
        if (!r && adc >= OPEN_LIM)
            return OHMETER_OPEN;

        // If we're on the last (lowest R) range and have a very low
        // reading, it's short
        if (ranges[r+1].r == 0 && adc <= SHORT_LIM)
            return OHMETER_SHORT;

        float floatval = adc / 4095.0; // Full range
        // Avoid some divide by zero messes, that shouldn't
        // normally happen; although might if someone unplugs
        // between ranges
        if (floatval == 0.0)
            floatval = 1.0/100000.0;
        else if (floatval == 1.0) // Full range
            floatval -= 1.0/100000.0;

        float invfloat = 1.0/floatval;
        res = ranges[r].r * 1.0/(invfloat - 1.0);

        printf("Range: %d/%d raw res: %f\n", r, ranges[r].r, res);

        // If we're the last range, return it
        if (ranges[r+1].r == 0 )
            break;

        // Heuristic: If the value is lower than a multiple of
        // the next range's ref, then try the next range, else
        // return ours
        // Heuristic: If our raw value is very low, try the next
        // range, we tend to be inaccurate at the low end
        if ((res > ranges[r+1].r * NEXTRANGE_RATIO) &&
            (adc >= NEXTRANGE_LOW))
            break;
    }

    return (uint32_t)res;
}

