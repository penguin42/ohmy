/**
 * SPDX-License-Identifier: BSD-3-Clause
 * (c) David Alan Gilbert 2021, dave@treblig.org
 */

#include <stdbool.h>

#include "defs.h"
#include "speech_words.h"

void speak_number(uint32_t val)
{
  bool spoken = false;
  struct power {
    uint32_t  multiple;
    const word_data *power_word;
  };
  struct power powers[] = {
    { 1000000000, &word_billion },
    { 1000000,    &word_million },
    { 1000,       &word_thousand },
    { 100,        &word_hundred },
    {0,          &word_0 } /* dummy */
  };
  word_data *smalls[] = {
    &word_0, &word_1, &word_2, &word_3, &word_4, &word_5, &word_6, &word_7, &word_8, &word_9,
    &word_10, &word_11, &word_12, &word_13, &word_14, &word_15, &word_16, &word_17, &word_18, &word_19
  };
  word_data *tens[] = {
    &word_0, &word_10, &word_20, &word_30, &word_40, &word_50, &word_60, &word_70, &word_80, &word_90
  };
  for(int i = 0; powers[i].multiple != 0; i++) {
    if (val >= powers[i].multiple) {
      speak_number(val / powers[i].multiple);
      add_sound(powers[i].power_word);
      val = val % powers[i].multiple;
      spoken = true;
    }
  };

  if (val == 0) {
    if (!spoken) add_sound(&word_0);

    /* In the 'spoken' case (e.g. 1000) we don't need to add anything */
    return;
  }

  if (spoken) add_sound(&word_and);
  
  if (val > 19) {
    add_sound(tens[val / 10]);
    if (val % 10) add_sound(smalls[val % 10]);
  } else {
    add_sound(smalls[val]);
  }
}
