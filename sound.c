/**
 * SPDX-License-Identifier: BSD-3-Clause
 * (c) David Alan Gilbert 2021, dave@treblig.org
 */

#include <stdbool.h>
#include <stdint.h>

#include "pico/stdlib.h"
#include "hardware/dma.h"
#include "hardware/pwm.h"

#include "defs.h"

static const uint SOUND_PIN = 16; // bottom of the board

int sound_dma_data_chan = -1;
int sound_dma_ctrl_chan = -1;

// Straight ramp up to 0xff - PWM solid on, no switching noise
// but hopefully no click
static uint32_t sound_off_data[128];
word_data sound_off = { .data = sound_off_data, .len = 128, .name = "sound off" };
// SOund on, hopefully with no click
static uint32_t sound_on_data[128];
word_data sound_on = { .data = sound_on_data, .len = 128, .name = "sound on" };

// Note we need to leave a space at the end for a 0
// i.e. index 31 should be unused
#define MAX_SOUND_CBS 32

// Note the order/size here has to match the DMA alias 3 layout
struct {uint32_t len; const uint32_t *data;} sound_cbs[MAX_SOUND_CBS];
static uint32_t sound_cbs_idx = 0;

void clear_sounds(void)
{
  sound_cbs_idx = 0;
}

void add_sound(const word_data *w)
{
  if (sound_cbs_idx >= (MAX_SOUND_CBS - 1))
    return;

  sound_cbs[sound_cbs_idx].len = w->len;
  sound_cbs[sound_cbs_idx].data = w->data;
  sound_cbs_idx++;
}

void play_sound_queue(void)
{
  // We need room at the end for a NULL
  if (sound_cbs_idx >= MAX_SOUND_CBS)
    return;

  // This triggers an irq from the data DMA at the end
  sound_cbs[sound_cbs_idx].len = 0;
  sound_cbs[sound_cbs_idx].data = NULL;

  // Set it back to the start of the control blocks and start
  dma_channel_set_read_addr(sound_dma_ctrl_chan, sound_cbs, true /* trigger */);
}

void setup_sound(void)
{
    pwm_config sound_config = pwm_get_default_config();

    // each GPIO is assigned a particular PWM slice
    uint pwm_slice = pwm_gpio_to_slice_num(SOUND_PIN);
    //uint pwm_channel = pwm_gpio_to_channel(SOUND_PIN);

    // doc 4.5.2.2 gives an example saying that for a TOP of 254, 255 is 100%
    pwm_config_set_wrap(&sound_config, 254);

    gpio_set_dir(SOUND_PIN, GPIO_OUT);
    gpio_set_drive_strength(SOUND_PIN, GPIO_DRIVE_STRENGTH_12MA); // Full power!
    gpio_set_function(SOUND_PIN, GPIO_FUNC_PWM);

    pwm_init(pwm_slice, &sound_config, true);

    // Lets set a DC value for now - looks like you have to set both at once
    // so set both the same
    pwm_set_both_levels(pwm_slice, 33, 33);

    sound_dma_data_chan = dma_claim_unused_channel(true);
    sound_dma_ctrl_chan = dma_claim_unused_channel(true);

    // (Based on the RP2040 section 2.5.6.2 DMA Control blocks example code)
    // Set up the parameters for the control channel first
    // each cycle of control channel writes a (len+data) into the data
    // channel on the alias that triggers the data DMA on the 2nd write
    dma_channel_config cc = dma_channel_get_default_config(sound_dma_ctrl_chan);
    channel_config_set_transfer_data_size(&cc, DMA_SIZE_32);
    channel_config_set_read_increment(&cc, true);
    channel_config_set_write_increment(&cc, true);
    // Wrap write pointer at 2^3=8 bytes (len+addr)
    channel_config_set_ring(&cc, true, 3);
    dma_channel_configure(
        sound_dma_ctrl_chan,
        &cc,
        &dma_hw->ch[sound_dma_data_chan].al3_transfer_count, // Write address
        sound_cbs,   // read address
        2,           // Count - one control block
        false        /* dont trigger yet */
    );

    dma_channel_config dc = dma_channel_get_default_config(sound_dma_data_chan);
    channel_config_set_transfer_data_size(&dc, DMA_SIZE_32);
    channel_config_set_read_increment(&dc, true);
    channel_config_set_write_increment(&dc, false);
    channel_config_set_chain_to(&dc, sound_dma_ctrl_chan); // Kick ctrl when done
    channel_config_set_irq_quiet(&dc, true); // Only IRQ on 0 trigger - end of chain

    // Using a pacing timer with the PWM seems odd, there doesn't seem to be
    // an easy way to DMA at a fraction of the PWM speed; maybe forcing through
    // a dummy DMA or PIO
    dma_hw->timer[0] = (1 << 16) | 5000; // 125MHz / 5000->25kHz sample
    channel_config_set_dreq(&dc, DREQ_DMA_TIMER0);

    dma_channel_configure(
        sound_dma_data_chan,
        &dc,
        &pwm_hw->slice[pwm_slice].cc, // write address
        NULL,   // read address - dummy, gets set by control
        0,      // Count - dummy, gets set by control
        false /* trigger  - not yet */
    );

    for(int i=0; i<128; i++) {
      uint32_t data = i+0x80;
      data |= data << 16;
      // sound_off is a ramp from 0x80 ... 0xff
      sound_off_data[i] = data;
      // sound on is a ramp back from 0xff ... 0x80
      sound_on_data[127-i] = data;
    }
}

bool sound_busy(void)
{
#if 0
        // I've not been able to get this test to work, so wait until
        // we run out of the data we sent it
        while (!dma_channel_get_irq0_status(sound_dma_data_chan))
          tight_loop_contents();
    
        dma_channel_acknowledge_irq0(sound_dma_data_chan);
#endif
  return dma_hw->ch[sound_dma_ctrl_chan].read_addr < (uintptr_t)&(sound_cbs[sound_cbs_idx].data);
}

