/**
 * SPDX-License-Identifier: BSD-3-Clause
 * (c) David Alan Gilbert 2021, dave@treblig.org
 */

#include "defs.h"
#include "speech_words.h"

#include "speech_words.data"
