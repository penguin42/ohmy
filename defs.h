/**
 * SPDX-License-Identifier: BSD-3-Clause
 * (c) David Alan Gilbert 2021, dave@treblig.org
 */

#include <stdbool.h>
#include <stdint.h>

#ifndef DEFS_HEADER
#define DEFS_HEADER

typedef struct {
  const uint32_t *data;
  const uint32_t  len;
  const char     *name;
} word_data;


// clear the queue, then add words to it, and then play
void setup_sound(void);
void clear_sounds(void);
void add_sound(const word_data *w);
void play_sound_queue(void);
bool sound_busy(void);
// On/off ramps with no click
extern word_data sound_on;
extern word_data sound_off;

void speak_number(uint32_t val);

#define OHMETER_OPEN  0xffffffffu
#define OHMETER_SHORT 0xfffffffeu

void ohmeter_init(void);
uint32_t read_ohmeter(void);

#endif
